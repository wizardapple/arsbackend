let mongoose = require('mongoose')

var MatchType = new mongoose.Schema({
    uid: {
        type: String,
        required: true,
    },
    similarityRating: {
        type: Number,
        required: true,
    }
})

module.exports = MatchType