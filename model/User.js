let mongoose = require('mongoose')

var MatchType = require('./MatchType')

const numberRequired = { type: Number, required: true }

var userSchema = new mongoose.Schema({
    uid: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    grade: numberRequired,
    residency: numberRequired,
    app_loc: numberRequired,
    field_proximity: numberRequired,
    major: numberRequired,
    gen_area: numberRequired,
    availability: numberRequired,
    gender: numberRequired,
    gender_id: numberRequired,
    finaid: numberRequired,
    intl_region: numberRequired,
    school_sys: numberRequired,
    sm_reddit: numberRequired,
    sm_email: numberRequired,
    sm_discord: numberRequired,
    sm_skype: numberRequired,
    sm_whatsapp: numberRequired,
    sm_cellphone: numberRequired,
    sm_insta: numberRequired,
    applying_to_t20: numberRequired,
    applying_to_public: numberRequired,
    applying_to_private: numberRequired,
    applying_to_lac: numberRequired,
    race_white: numberRequired,
    race_african_american: numberRequired,
    race_american_indian: numberRequired,
    race_asian_indian: numberRequired,
    race_chinese: numberRequired,
    race_filipino: numberRequired,
    race_asian: numberRequired,
    race_japanese: numberRequired,
    race_korean: numberRequired,
    race_vietnamese: numberRequired,
    race_native_hawaiian: numberRequired,
    race_middle_eastern: numberRequired,
    race_guam_chamorro: numberRequired,
    race_samoan: numberRequired,
    race_other_pi: numberRequired,
    race_mexican_american: numberRequired,
    race_puerto_rican: numberRequired,
    race_cuban: numberRequired,
    race_hispanic: numberRequired,
    matches: [MatchType],
    pairedUser: String,
    matchConfirmed: {
        type: Boolean,
        required: true
    }
})

var User = mongoose.model('User', userSchema)
module.exports = User