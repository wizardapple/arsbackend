let csvJ = require('csvtojson')
let randomUsername = require('../matching/util').generateUsername
let User = require('../model/User')

var management = require('./management')

function createUserAuth(email, username) {
    var userReq = {
        "email": email,
        "email_verified": false,
        "name": username,
        "password": randomUsername() + (Math.random() * 1000000),
        "verify_email": false,
        "connection": "Username-Password-Authentication",
        "nickname": username
    }
    return management.createUser(userReq)
}


function createUser(csv) {
    return csvJ().fromString(csv).then(users => {
        users.forEach((usrObj, idx) => {
            setTimeout(function () {
                var username = randomUsername()
                createUserAuth(usrObj.email, username).then(userData => {
                    var usr = Object.assign({ uid: userData.user_id, username: username }, usrObj)
                    delete usr['email']
                    usr.matchConfirmed = false
                    var usrDB = new User(usr)
                    return usrDB.save()
                }).then((err) => {
                    if (err) console.error(err)
                    console.log(`Created user ${usrObj.email}`)
                })
            }, 500 * idx)
        })
    })
}

module.exports = createUser