# Endpoints

## All Endpoints
### Required Headers
- `Authorization`: Valid Auth0 JWT
- `Origin`: Must be an ARSMatrix authorized domain

## `user` Namespace
### `/user/:uid/profile`
- Get the profile of a user based on their UID.
### `/user/:uid/vector`
- Get the vector of a user based on their UID.
### `/user/:uid/updateProfile`
- Update a user's profile based on request body JSON.
#### Required Headers
- `Content-Type`: `application/json`
- Request body: JSON object of the keys and values you want to update