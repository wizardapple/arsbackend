# Database Schema

**DB Name: arsmatrix**

## Collection: `users`
Each `User` consists of:

| Field Name | Data Type | Required? | Description | Perms | Possible Values |
|------------|-----------|-----------|-------------|-------|---|
| `uid` | `String` | Yes | Auth0 user ID | Mutable by admin only | any lowercase |
| `username` | `String` | Yes | Name of user | Mutable by user | any unique |
| `grade` | `Number` | Yes | Grade (11, 12) | Mutable by user | `11`, `12` (client-enforced) |
| `residency` | `Number` | Yes | Residency status | Mutable by user | `1-3` (client-enforced) |
| `app_loc` | `Number` | Yes | Number showing which schools are being applied to | Mutable by user | `2-30` (client-enforced) |
| `field_proximity` | `Number` | Yes | Indicator of the specificity level of advice needed | Mutable by user | `1`, `20`, `300` (client-enforced) |
| `major` | `Number` | Yes | College Board major code | Mutable by user | Numerous (client-enforced) |
| `gen_area` | `Number` | Yes | College Board major code for general area | Mutable by user | Numerous (client-enforced) |
| `availability` | `Number` | Yes | Availability rating | Mutable by user | `1`-`5` (client-enforced) |
| `gender` | `Number` | Yes | Gender | Mutable by user | `1`-`5` (client-enforced) |
| `gender_id` | `Number` | Yes | Gender identity | Mutable by user | `1`-`8` (client-enforced) |
| `finaid` | `Number` | Yes | Financial aid interest | Mutable by user | `1`-`3` (client-enforced) |
| `intl_region` | `Number` | Yes | International region | Mutable by user | Various (client-enforced) |
| `school_sys` | `Number` | Yes | School system | Mutable by user | `1`-`8` (client-enforced) |
| `sm_reddit` | `Number` | Yes | Prefers Reddit for contact | Mutable by user | `0`-`1` (client-enforced) |
| `sm_email` | `Number` | Yes | Prefers email for contact | Mutable by user | `0`-`1` (client-enforced) |
| `sm_discord` | `Number` | Yes | Prefers Discord for contact | Mutable by user | `0`-`1` (client-enforced) |
| `sm_skype` | `Number` | Yes | Prefers Skype for contact | Mutable by user | `0`-`1` (client-enforced) |
| `sm_whatsapp` | `Number` | Yes | Prefers WhatsApp for contact | Mutable by user | `0`-`1` (client-enforced) |
| `sm_cellphone` | `Number` | Yes | Prefers SMS/cell phone for contact | Mutable by user | `0`-`1` (client-enforced) |
| `sm_insta` | `Number` | Yes | Prefers Instagram for contact | Mutable by user | `0`-`1` (client-enforced) |
| `applying_to_t20` | `Number` | Yes | Applying/applied to T20 schools | Mutable by user | `0`-`1` (client-enforced) |
| `applying_to_public` | `Number` | Yes | Applying/applied to public schools | Mutable by user | `0`-`1` (client-enforced) |
| `applying_to_lac` | `Number` | Yes | Applying/applied to Libral Arts schools | Mutable by user | `0`-`1` (client-enforced) |
| `race_white` | `Number` | Yes | Is white | Mutable by user | `0`-`1` (client-enforced) |
| `race_african_american` | `Number` | Yes | Is African-American | Mutable by user | `0`-`1` (client-enforced) |
| `race_american_indian` | `Number` | Yes | Is American Indian | Mutable by user | `0`-`1` (client-enforced) |
| `race_asian_indian` | `Number` | Yes | Is Asian Indian | Mutable by user | `0`-`1` (client-enforced) |
| `race_chinese` | `Number` | Yes | Is Chinese | Mutable by user | `0`-`1` (client-enforced) |
| `race_filipino` | `Number` | Yes | Is Filipino | Mutable by user | `0`-`1` (client-enforced) |
| `race_asian` | `Number` | Yes | Is Asian | Mutable by user | `0`-`1` (client-enforced) |
| `race_japanese` | `Number` | Yes | Is Japanese | Mutable by user | `0`-`1` (client-enforced) |
| `race_korean` | `Number` | Yes | Is Korean | Mutable by user | `0`-`1` (client-enforced) |
| `race_vietnamese` | `Number` | Yes | Is Vietnamese | Mutable by user | `0`-`1` (client-enforced) |
| `race_native_hawaiian` | `Number` | Yes | Is Native Hawaiian | Mutable by user | `0`-`1` (client-enforced) |
| `race_middle_eastern` | `Number` | Yes | Is Middle Eastern | Mutable by user | `0`-`1` (client-enforced) |
| `race_guam_chamorro` | `Number` | Yes | Is Guam/Chamorro | Mutable by user | `0`-`1` (client-enforced) |
| `race_samoan` | `Number` | Yes | Is Samoan | Mutable by user | `0`-`1` (client-enforced) |
| `race_other_pi` | `Number` | Yes | Is Other Pacific Islander | Mutable by user | `0`-`1` (client-enforced) |
| `race_mexican_american` | `Number` | Yes | Is Mexican-American | Mutable by user | `0`-`1` (client-enforced) |
| `race_puerto_rican` | `Number` | Yes | Is Puerto Rican | Mutable by user | `0`-`1` (client-enforced) |
| `race_cuban` | `Number` | Yes | Is Cuban | Mutable by user | `0`-`1` (client-enforced) |
| `race_hispanic` | `Number` | Yes | Is Hispanic | Mutable by user | `0`-`1` (client-enforced) |
| `matches` | `[MatchType]` | No | An array of potential matches. | Mutable by server only | Varies (dual-enforced) |
| `pairedUser` | `String` | No | The UID of the user this user has chosen to pair with (may or may not be confirmed). | Mutable by server only | Varies (client-enforced) |
| `matchConfirmed` | `Boolean` | Yes | Whether or not a match has been confirmed. | Mutable by user | Varies (client-enforced) |

### `residency` Value Mappings
- 1 = US
- 2 = Canada
- 3 = Int'l

### `app_loc` Value Mappings
- Add on numbers to the base to create composite value
- +2 = US schools
- +4 = Canadian schools
- +10 = UK schools
- +14 = International schools

### `field_proximity` Value Mappings
- 1 = specialized
- 20 = general area
- 300 = pair with anyone

### `major` Value Mappings
- Accounting & Finance = 201
- Aeronautical & Manufacturing Engineering = 451
- Agriculture & Forestry = 100
- American Studies = 140
- Anatomy & Physiology = 166
- Anthropology = 901
- Archaeology = 902
- Architecture = 121
- Art & Design = 944
- Aural & Oral Sciences = 870
- Biomedical Engineering = 454
- Biological Sciences = 161
- Building = 390
- Business & Management Studies = 200
- Celtic Studies = 142
- Chemical Engineering = 455
- Chemistry = 836
- Civil Engineering = 456
- Classics & Ancient History = 702
- Communication & Media Studies = 252
- Complementary Medicine = 682
- Computer Science = 303
- Counselling = 433
- Creative Writing = 522
- Criminology = 903
- Dentistry = 627
- Drama = 943
- Dance & Cinematics = 942
- East & South Asian Studies = 142
- Economics = 904
- Education = 400
- Electrical & Electronic Engineering = 459
- English = 520
- Engineering (General) = 450
- Fashion = 945
- Film Making = 946
- Food Science = 107
- Forensic Science = 890
- French = 556
- Geography & Environmental Sciences = 837
- Geology = 837
- German = 557
- History = 700
- History of Art = 941
- Architecture & Design = 121
- Hospitality = 211
- Leisure = 800
- Recreation & Tourism = 802
- Iberian Languages/Hispanic Studies = 142
- Italian = 558
- International Relations = 906
- Journalism = 254
- Land & Property Management = 200
- Law = 712
- Librarianship & Information Management = 730
- Linguistics = 550
- Literature = 520
- Marketing = 218
- Materials Technology = 466
- Mathematics = 742
- Mechanical Engineering = 467
- Medical Technology = 617
- Medicine = 628
- Middle Eastern & African Studies = 142
- Music Performance = 952
- Music Studies = 950
- Nursing = 619
- Occupational Therapy = 678
- Optometry = 680
- Ophthalmology & Orthoptics = 680
- Pharmacology & Pharmacy = 629
- Philosophy = 821
- Physics and Astronomy = 833
- Physiotherapy = 682
- Politics = 907
- Psychology = 870
- Robotics = 516
- Russian & East European Languages = 560
- Social Policy = 883
- Social Work = 884
- Sociology = 908
- Sports Science = 603
- Theology & Religious Studies = 822
- Town & Country Planning and Landscape Design = 123
- Veterinary Medicine = 630
- Youth Work = 884
- None/Undecided = 999

### `gen_area` Value Mappings
- Agriculture = 100
- Business = 200
- Engineering = 450
- General Arts (not to be confused with social sciences or liberal arts) = 940
- Health and Medicine = 600
- Law = 710
- Liberal Arts = 720
- Multi-/Interdisciplinary Studies = 770
- Performing Arts = 940
- Public and Social Services = 880
- Sciences = 830
- Social Sciences = 900
- Technology = 300
- Trades and Personal Services = 390
- None/Undecided = 999

### `availability` Value Mappings
- Scale goes from 1 (low availability) to 5 (high availability).

### `gender` Value Mappings
- 1 = male
- 2 = female
- 3 = trans
- 4 = neutral
- 5 = other

### `gender_id` Value Mappings
- 1 = straight
- 5 = gay/lesbian
- 6 = bisexual
- 7 = asexual
- 8 = spectrum/other

### `finaid` Value Mappings
- 1 = yes
- 2 = no
- 3 = maybe

### `intl_region` Value Mappings
- 0 = domestic/region not listed
- Central America = 240
- Mexico = 200
- Caribbean = 250
- South America = 270
- Eastern Africa = 300
- Middle Africa (Congo) = 310
- Northern Africa = 320
- Southern Africa = 330
- Western Africa (Guinea) = 340
- Middle East = 400
- Eastern Europe – inc. North Asia (Siberia) and Central Europe = 500
- Northern Europe – inc. British Isles = 510
- Southern Europe = 520
- Western Europe – inc. Germany and other DACH countries = 530
- Southeast Asia = 600
- East Asia = 610
- South Asia = 620
- Central Asia = 630
- Western Asia = 640
- Australia and New Zealand – inc. Elizabeth and Middleton Reefs, Lord Howe Island Group, Norfolk Island = 700
- Melanesia – inc. New Caledonia and New Guinea = 800
- Micronesia = 830
- Polynesia = 860

### `school_sys` Value Mappings
- 1 = AP
- 2 = IB
- 3 = A-Levels
- 4 = IG
- 5 = ECF
- 6 = 100 Point Scale
- 7 = GPA
- 8 = Other