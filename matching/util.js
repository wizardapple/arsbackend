let math = require('mathjs'),
    animals = require('animals'),
    superb = require('superb')


function titleCase(str) {
    str = str.toLowerCase().split(' ');
    for (var i = 0; i < str.length; i++) {
        str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
    }
    return str.join(' ');
}

var util = {}

util.cosineSimilarity = function (a, b) {
    x = math.matrix(a), y = math.matrix(b)
    var numerator = math.dot(x, y),
        denominator = math.multiply(math.norm(x), math.norm(y))
    return math.divide(numerator, denominator)
}

util.generateUsername = function () {
    var anim = titleCase(animals()),
        supers = titleCase(superb.random()),
        num = Math.round(Math.random() * 1000)
    return `${supers} ${anim} #${num}`
}

module.exports = util