// var User = require('./model/User')
var Database = require('./model/Database')
let express = require('express')
let bodyParser = require('body-parser')
let cors = require('cors')
let jwt = require('express-jwt')
let jwtAuthz = require('express-jwt-authz')
let jwksRsa = require('jwks-rsa')

var userRouter = require('./routes/user')
var adminRouter = require('./routes/admin')

require('dotenv').config()

var app = express()

let checkJwt = jwt({
	secret: jwksRsa.expressJwtSecret({
		cache: true,
		rateLimit: true,
		jwksRequestsPerMinute: 5,
		jwksUri: `https://arsmatrix.auth0.com/.well-known/jwks.json`
	}),
	audience: 'https://localhost:8000',
	issuer: `https://arsmatrix.auth0.com/`,
	algorithms: ['RS256']
})
app.use(checkJwt)
app.use((err, req, res, next) => {
	if (err.name === 'UnauthorizedError') {
		res.status(err.status).json({ error: err.message })
		res.end()
		return
	}
	next()
})

let whitelist = ['http://localhost:3000']
let corsOptions = {
	origin: function (origin, callback) {
		if (whitelist.indexOf(origin) !== -1) {
			callback(null, true)
		} else {
			callback('not-allowed')
		}
	}
}

app.use(cors(corsOptions))

app.use(bodyParser.json())

app.use('/user', userRouter)
app.use('/admin', adminRouter)

app.get('/', (req, res) => {
	res.send('ARSMatrix Backend Server v1.0')
})

app.listen(8000, () => {
	console.log('Listening on port 8000...')
})