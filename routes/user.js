var express = require('express')
var app = express.Router()

var User = require('../model/User')

var getVector = require('../matching/getVector')

app.get('/:uid/profile', (req, res, next) => {
	console.log('Request made:', req.params.uid)
	if (!req.params.uid) {
		res.status(400).json({ error: 'no-uid-specified' })
	} else {
		User.findOne({ uid: req.params.uid }).then(doc => {
			res.status(200).json(doc.toJSON({ versionKey: false }))
		}).catch(err => {
			console.error('Not found:', req.params.uid)
			res.status(404).json({ error: 'not-found' })
		})
	}
})

app.get('/:uid/vector', (req, res, next) => {
	console.log('Request made:', req.params.uid)
	if (!req.params.uid) {
		res.status(400).json({ error: 'no-uid-specified' })
	} else {
		User.findOne({ uid: req.params.uid }).then(getVector).then(vec => {
			res.status(200).json(vec)
		}).catch(err => {
			console.error('Not found:', req.params.uid)
			console.log(err)
			res.status(404).json({ error: 'not-found' })
		})
	}
})

app.post('/:uid/updateProfile', (req, res) => {
	if (!req.params.uid) {
		res.status(400).json({ error: 'no-uid-specified' })
	} else if (!req.body) {
		res.status(400).json({ error: 'no-update-values-specified' })
	} else if (req.user.sub !== req.params.uid) {
		res.status(401).send({ error: 'not-allowed' })
	} else if (Object.keys(req.body).includes('uid') || Object.keys(req.body).includes('matches') || Object.keys(req.body).includes('matchConfirmed')) {
		res.status(401).send({ error: 'unauthorized-update' })
	} else {
		User.findOne({ uid: req.params.uid }).then(usr => {
			usr.set(req.body).save().then(() => {
				res.status(200).send({ status: 'done' })
			})
		})
	}
})

module.exports = app