var express = require('express')
var bodyParser = require('body-parser')
var app = express.Router()

var management = require('../auth/management'),
    createUser = require('../auth/createUser'),
    User = require('../model/User')

var csvParser = bodyParser.text({ type: 'text/csv' })

app.post('/csvImport', csvParser, (req, res) => {
    createUser(req.body)
    res.json({ status: 'import job started' })
})

// app.get('/deleteAllUsers', (req, res) => {
//     management.getUsers({ per_page: 100 }).then(users => {
//         return Promise.all(users.map(usr => {
//             return management.deleteUser({ id: usr.user_id })
//         }))
//     })
//         .then(() => {
//             return new Promise((resolve, reject) => {
//                 User.deleteMany({}, resolve)
//             })
//         })
//         .then(() => res.json({ status: 'done' }))
// })

module.exports = app